<%@ page import="java.util.List" %>
<%@ page import="utils.AWSS3Utils" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>List of Photos in Album</title>
</head>
<body>
<%
  List<String> objectKeys = AWSS3Utils.getAlbumPhotoKeys();
%>

<ul>
<%
  for (String objectKey : objectKeys) {
%>
  <li>
    <p><%=objectKey%></p>
    <img src="<%=AWSS3Utils.getS3ObjectUrl(objectKey)%>"/>
  </li>
<%
  }
%>

</ul>
</body>
</html>
