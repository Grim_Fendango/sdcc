package servlets;

import java.io.*;
import java.nio.file.Paths;
import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import utils.AWSS3Utils;


/**
 * Handles the file upload requests.
 */
@WebServlet("/album-photos")
@MultipartConfig
public class AlbumPhotosServlet extends HttpServlet {

    /**
     * Adds a new photo to the album, given a valid image file.
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        if(AWSS3Utils.uploadFileToS3(filePart, fileName)) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("album.jsp");
            dispatcher.forward(request, response);
        }
        response.setStatus(400);
    }
}
