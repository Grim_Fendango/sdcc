package utils;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utils used to conveniently store album photos and retrieve links and keys to existing album photos
 * currently stored in the s3 bucket used by the application.
 */
public class AWSS3Utils {

    private static final AWSConfig config = new AWSConfig();

    /**
     * Returns s3 keys to all photos in the album
     */
    public static List<String> getAlbumPhotoKeys() {

        AmazonS3 client = new AmazonS3Client(new BasicAWSCredentials(config.getAwsAccessKey(), config.getAwsSecretKey()));

        // get list of files
        List<String> objectKeys = new ArrayList<>();

        ObjectListing objects = client.listObjects(config.getBucket());
        do {
            objectKeys.addAll(objects.getObjectSummaries().stream()
                    .map( object -> object.getKey())
                    .filter(object -> isValidFileExtension(FilenameUtils.getExtension(object)))
                    .collect(Collectors.toList()));
            objects = client.listNextBatchOfObjects(objects);
        } while (objects.isTruncated());
        // get list of file references

        return objectKeys;
    }

    /**
     * @param objectKey reference to an image object stored within the photo album s3 bucket.
     * @return
     */
    public static String getS3ObjectUrl(String objectKey){
        return "http://s3-" + config.getRegion() +".amazonaws.com/" + config.getBucket() +"/" + objectKey;
    }

    /**
     * @param filePart The content of the form element that contains the file that has been uploaded.
     * @param fileName The name of the file that has been uploaded
     * @return True: if file upload was successful, False: if upload was unsuccessful
     * @throws IOException
     */
    public static boolean uploadFileToS3(Part filePart, String fileName) throws IOException {
        String fileExtension = FilenameUtils.getExtension(fileName);
        if (!isValidFileExtension(fileExtension)) {
            return false;
        }
        InputStream fileContent = filePart.getInputStream();
        AmazonS3 client = new AmazonS3Client(new BasicAWSCredentials(config.getAwsAccessKey(), config.getAwsSecretKey()));
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentLength(filePart.getSize());
        meta.setContentType("application/" + fileExtension);
        client.putObject(config.getBucket(), fileName, fileContent, meta);
        client.setObjectAcl(config.getBucket(), fileName, CannedAccessControlList.PublicRead);
        return true;
    }

    /**
     * @param fileExtension A file extension (eg., exe)
     * @return True: if the file extension is one of the permisable image file extensions (i.e., jpg, bmp or png).
     */
    private static boolean isValidFileExtension(String fileExtension) {
        if (!fileExtension.isEmpty()) {
            if (fileExtension.equals("jpg") || fileExtension.equals("bmp") || fileExtension.equals("png")) {
                return true;
            }
        }
        return false;
    }
}
