package utils;

/**
 * Contains the AWS configuration information necessary to establish a connection with a AWS
 * S3 bucket. In future implementations this could be read from a configuration file instead of
 * being hard coded values.
 */
public class AWSConfig {

    private static final String bucket = "sddc-bucket-1";
    private static final String awsAccessKey = "XXXXX"; // TODO: get this from a config file
    private static final String awsSecretKey = "YYYYY"; // TODO: get this from a config file
    private static final String region = "ap-southeast-2";

    /**
     * @return The name of the s3 bucket that contains the album photos that is used by the applicaiton.
     */
    public String getBucket() {
        return bucket;
    }

    public String getAwsAccessKey() {
        return awsAccessKey;
    }

    public String getAwsSecretKey() {
        return awsSecretKey;
    }

    /**
     * @return The region that the s3 bucket was registered in. This is used for the construction of
     * s3 object urls.
     */
    public String getRegion() {
        return region;
    }
}
