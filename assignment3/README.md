#SDCC Assignment 3


#Instructions

##First time installation
1. Navigate to source/main/resources
2. Make a copy of aws-secrets.properties.tmpl called aws-secrets.properties
3. Fill in the empty property values to match your aws credentials.
4. Make a copy of db-connection.properties.tmpl called db-connection.properties
5. Fill in the empty fields (connection string and credentials)

##Create a War File

1. Run the following command:
	
	```
	./gradlew war
	```
2. The war file output can be found at:

	```
	build/libs/assignment3.war
	```	
	
This war file is only compatable with tomcat 8. Jetty has not been tested.


##Run the application from gradle

Running the program from gradle (the preferred choice) can be done by running the following command:

```
./gradlew bootRun
```	

Then go to ```localhost:8080```