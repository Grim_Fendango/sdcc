package com.sdcc.assignment3.data;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * This interface is used by spring data to create templated ORM CRUD methods.
 */
public interface PhotoDescriptionRepository extends CrudRepository<PhotoDescription, String> {
    List<PhotoDescription> findAll();
    PhotoDescription save(PhotoDescription description);
}
