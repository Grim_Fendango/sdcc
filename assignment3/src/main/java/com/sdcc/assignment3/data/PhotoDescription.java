package com.sdcc.assignment3.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * ORM entity data model for photo description
 */
@Table(name="photo_description")
@Entity
public class PhotoDescription {

    @Id
    @Column(length = 40)
    private String photoName;

    @Column(length = 100)
    private String description;

    public PhotoDescription() {}

    public PhotoDescription(String photoName, String description) {
        this.photoName = photoName;
        this.description = description;
    }

    public String getPhotoName() {
        return photoName;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "PhotoDescription{" + "photoName='" + photoName + '\'' + ", description='" + description + '\'' + '}';
    }
}
