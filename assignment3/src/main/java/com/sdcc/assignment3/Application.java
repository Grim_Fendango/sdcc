package com.sdcc.assignment3;

import com.sdcc.assignment3.data.PhotoDescription;
import com.sdcc.assignment3.data.PhotoDescriptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor;
import org.springframework.web.WebApplicationInitializer;

/**
 * Spring boot configuration
 */
@EnableJpaRepositories
@SpringBootApplication
public class Application extends SpringBootServletInitializer implements WebApplicationInitializer {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    @Override
	public SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application
                .properties("spring.config.name=db-connection,application,aws-secrets")
                .sources(Application.class);
	}

	public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)
                .properties("spring.config.name=db-connection,application,aws-secrets")
                .run(args);
	}

    @Bean
    public PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor()
    {
        PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor = new PersistenceAnnotationBeanPostProcessor();
        return persistenceAnnotationBeanPostProcessor;
    }
}
