package com.sdcc.assignment3.controller;

import com.sdcc.assignment3.Application;
import com.sdcc.assignment3.config.AwsSecretProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Maps index requests to the relevant views.
 */
@Controller
public class IndexController {

    @Autowired
    private AwsSecretProperties properties;


    @RequestMapping("/")
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        return mav;
    }
}
