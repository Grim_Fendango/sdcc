package com.sdcc.assignment3.controller;

import com.sdcc.assignment3.data.PhotoDescription;
import com.sdcc.assignment3.data.PhotoDescriptionRepository;
import com.sdcc.assignment3.service.AwsS3FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@MultipartConfig
public class AlbumPhotoController {

    private final AwsS3FileService awsS3FileService;
    private final PhotoDescriptionRepository photoDescriptionRepository;

    @Autowired
    public AlbumPhotoController(AwsS3FileService awsS3FileService,
            PhotoDescriptionRepository photoDescriptionRepository) {
        this.awsS3FileService = awsS3FileService;
        this.photoDescriptionRepository = photoDescriptionRepository;
    }

    @RequestMapping(value="/album-photos", method = RequestMethod.GET)
    public ModelAndView albumPhotos() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("album-photos");

        //TODO: use actual view model instead
        Map<String, PhotoDescription> urlDescriptionMap =  photoDescriptionRepository.findAll().stream()
                .collect(Collectors.toMap(
                        description -> awsS3FileService.getS3ObjectUrl(description.getPhotoName()),
                        description ->  description));

        mav.addObject("objectUrlsDecriptionMap", urlDescriptionMap);
        return mav;
    }

    /**
     * Adds a new photo to the album, given a valid image file.
     * @throws ServletException
     * @throws IOException
     */
    // TODO: write this using spring mvc conventions
    @RequestMapping(value = "/album-photos", method = RequestMethod.POST)
    public String uploadPhoto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String description= request.getParameter("description");
        Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        if(awsS3FileService.uploadFileToS3(filePart, fileName)) {
            photoDescriptionRepository.save(new PhotoDescription(fileName, description));
            return "redirect:/album-photos";
        } else {
//            response.setStatus(400);
            return "redirect:/error/photo-error"; // TODO: show error page
        }
    }
}
