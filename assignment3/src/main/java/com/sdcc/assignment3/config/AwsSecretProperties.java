package com.sdcc.assignment3.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * Contains the AWS configuration information necessary to establish a connection with a AWS
 * S3 bucket. In future implementations this could be read from a configuration file instead of
 * being hard coded values.
 */
@Component
@ConfigurationProperties(prefix="aws")
public class AwsSecretProperties {

    private String awsAccessKeyId;

    private String awsSecretKey;

    private String bucket;

    private String region;

    public String getAwsAccessKey() {
        return awsAccessKeyId;
    }

    public String getAwsSecretKey() {
        return awsSecretKey;
    }

    /**
     * @return The name of the s3 bucket that contains the album photos that is used by the applicaiton.
     */
    public String getBucket() {
        return bucket;
    }

    /**
     * @return The region that the s3 bucket was registered in. This is used for the construction of
     * s3 object urls.
     */
    public String getRegion() {
        return region;
    }

    public void setAwsAccessKeyId(String awsAccessKeyId) {
        this.awsAccessKeyId = awsAccessKeyId;
    }

    public void setAwsSecretKey(String awsSecretKey) {
        this.awsSecretKey = awsSecretKey;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
