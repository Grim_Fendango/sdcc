<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>List of Photos in Album</title>
</head>
<body>

<ul>
  <c:forEach var="entry" items="${objectUrlsDecriptionMap}">
  <li>
    <p>Photo Name: ${entry.value.getPhotoName()}</p>
    <p>Description: ${entry.value.getDescription()}</p>
    <img src="${entry.key}"/>
  </li>
  </c:forEach>
</ul>
</body>
</html>
