<%--
  Created by IntelliJ IDEA.
  User: zakariahchitty
  Date: 16/08/2016
  Time: 9:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
  </head>
  <body>
  <form enctype="multipart/form-data" action="album-photos" method="post">
    <fieldset>
      <div>
        <label for="description" >Describe your photo:</label>
        <input name="description" type="text" id="description"/>
      </div>

      <div>
        <label for="file" >Select your photo:</label>
        <input name="file" type="file" id="file"/>
      </div>

      <input type="submit" value="Upload"/>
    </fieldset>
  </form>

  </body>
</html>
