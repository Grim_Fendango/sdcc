
#Produce jar file

Run the following:

```
gradle jar
```

#How to send files to EMR

Run the following:

```
keys scp -i <location of pem> <location of jar> hadoop@<dns name of EMR instance>:

```

#Source Dump

Run the following:

```
tail -n 3000 src/**/*.java > ~/dev/SDCC/projects/source-a4.txt
```