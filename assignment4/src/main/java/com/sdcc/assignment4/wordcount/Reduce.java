package com.sdcc.assignment4.wordcount;

import java.io.IOException;
import java.util.HashMap;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class Reduce extends Reducer<Text, Text, Text, Text> {

    /**
     * This reduce function takes a list of key value pairs that together denote the occurance of a particular word (key)
     * in a given input file (the value). This function aggregates this data into a string that contains the total
     * number of occurances of the given word both in terms of each respective input file and the total number of occurances
     * accross all files.
     * @param key The word that has been found
     * @param fileNames Iterable list of occurances of the key word in a given file. There is one file name entry in the
     *                  list for each occurance of the key word.
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    public void reduce(Text key, Iterable<Text> fileNames, Context context) throws IOException, InterruptedException {
        HashMap<String, Integer> fileNameCountMap = new HashMap();
        int total = 0;
        for (Text fileName : fileNames) {
            Integer sum = fileNameCountMap.containsKey(fileName) ? fileNameCountMap.get(fileName) + 1 : 1;
            fileNameCountMap.put(fileName.toString(), sum);
            total++;
        }

        // Produces the value string that contains the processed data
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        fileNameCountMap.forEach((fileName, count) -> stringBuilder.append(String.format(" %s=%d ", fileName, count)));
        stringBuilder.append("}\n");
        stringBuilder.append("Total Occurence of \"" + key.toString() + "\": " + total);

        // Outputs the result
        context.write(key, new Text(stringBuilder.toString()));
    }
}