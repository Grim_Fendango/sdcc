
package com.sdcc.assignment4.wordcount;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

public class Map extends Mapper<LongWritable, Text, Text, Text> {
    private String pattern= "^[a-z][a-z0-9]*$";

    /**
     * Consumes the input file data and produces a key value pair that represents the occurance of a word in a given
     * input file.
     * @param key
     * @param value The input file data
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String fileName = ((FileSplit)context.getInputSplit()).getPath().getName();
        StringTokenizer tokenizer = new StringTokenizer(line);
        while (tokenizer.hasMoreTokens()) {
            String stringWord = tokenizer.nextToken().toLowerCase();
            if (stringWord.matches(pattern)){
                context.write(new Text(stringWord), new Text(fileName));
            }
        }
    }
}